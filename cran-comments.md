## Response to your mail

Dear CRAN team,

thank you for your checking. You asked us to submit an updated version:

> Dear maintainer,
>
> Please see the problems shown on
> <https://cran.r-project.org/web/checks/check_results_dataquieR.html>.
>
> Please correct before 2021-09-08 to safely retain your package on CRAN.
>
> Do remember to look at the 'Additional issues'.
>
> The CRAN Team

Thank you for your mail. We have addressed the issue as described below.

## CRAN had stated the following problems before:

```
Version: 1.0.8
Check: tests
Result: ERROR
     Running ‘testthat.R’ [47s/71s]
    Running the tests in ‘tests/testthat.R’ failed.
    Last 13 lines of output:
     * _snaps/int_datatype_matrix/intdtv30000.svg
     * _snaps/int_datatype_matrix/intdtv40000.svg
     * _snaps/int_datatype_matrix/intdtv50000.svg
     * _snaps/int_datatype_matrix/integrity-datatype.svg
     * _snaps/print/app-ex-repsumtab.svg
     * _snaps/print/im-empty-repsumtab.svg
     * _snaps/print/im-ex1-repsumtab.svg
     * _snaps/print/im-ex2-repsumtab.svg
     * _snaps/pro_applicability_matrix/appmatrix-plot-for-segment-v10000-ok.svg
     * _snaps/pro_applicability_matrix/appmatrix-plot-ok.svg
     * _snaps/util_heatmap_1th/util-heatmap-1th-1.svg
     * _snaps/util_heatmap_1th/util-heatmap-1th-2.svg
     * _snaps/util_heatmap_1th/util-heatmap-1th-3.svg
     Error: Test failures
     Execution halted
Flavor: r-release-macos-arm64
```

Also:

```
M1mac noLD
```

Both platforms show the same problem:

```
── Failure (test-prep_study2meta.R:745:3): prep_study2meta works on study_data ──
  `guessed_meta_data` not equal to data.frame(...).
  Component "MISSING_LIST": 1 string mismatch
```

We have addressed these no-long-double related problems and made the heuristics
function `prep_study2meta` more robust in case of lower floating point accuracy.

## Test environments and R CMD check results

### Our developers checked on their local systems:

- local R installation, R 4.1.0, macOS 10.15.7
  `devtools::check(cran = TRUE)`
  
    `0 errors ✓ | 0 warnings ✓ | 0 notes ✓`
    
### We have run checks also on Apple Silicon at https://console.scaleway.com/
  `devtools::check(cran = TRUE)`
  
    `0 errors ✓ | 0 warnings ✓ | 0 notes ✓`

    
### win-builder, CI/CD services:

- Ubuntu on travis-ci, R 4.0.2 --> Ok.
- gitlab our own ci/cd image  --> Ok.
  - debian:bullseye and R 4.0.4 (x86_64-pc-linux-gnu (64-bit))
- gitlab our own ci/cd no-long-double image  --> Ok.
- win-builder (devel, stable, oldstable) --> (Ok, Ok, Ok)
- `devtools::check_rhub` --> All three platforms ("windows-x86_64-devel",
  "ubuntu-gcc-release", and "fedora-clang-devel") Ok.
- r-hub debian-gcc-devel-nold -- Ok.
- r-hub solaris-x86-patched -- Ok.
